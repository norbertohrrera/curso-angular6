import {
    reducerDestinosViajes,
    DestinosViajesState,
    NuevoDestinoAction, InitializeDestinosViajesState, InitMyDataAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        //setup
        const prevState: DestinosViajesState = InitializeDestinosViajesState();
        const action: InitMyDataAction= new InitMyDataAction(['destino 1', 'destino2']);
        //action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        //assertions (verificaciones)
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
        //tear down 
        //(sentencias necesarias para borrar el contenido que se haya ingresado en BD en la prueba)
    });

    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = InitializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});
